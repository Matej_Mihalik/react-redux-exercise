import { connect } from 'react-redux';
import AccountHistoryEmpty from './AccountHistoryEmpty';

function mapStateToProps(state) {
  return {
    accountListFetching: state.accountList.isFetching,
    accountListEmpty: !state.accountList.accounts.length
  };
}

export default connect(mapStateToProps)(AccountHistoryEmpty);
