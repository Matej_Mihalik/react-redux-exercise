import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './AccountHistoryEmpty.css';

class AccountHistoryEmpty extends Component {
  render() {
    const { accountListFetching, accountListEmpty } = this.props;
    const classes = classNames(
      'account-history-empty', {
        loading: accountListFetching,
        ready: !accountListEmpty
      }
    );

    return (
      <div className={classes} />
    );
  }
}

AccountHistoryEmpty.propTypes = {
  accountListFetching: PropTypes.bool.isRequired,
  accountListEmpty: PropTypes.bool.isRequired
};

export default AccountHistoryEmpty;
