import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { connect, Provider } from 'react-redux';
import PropTypes from 'prop-types';
import { getAccess } from 'actions/dbAccess';
import App from './App';

class AppContainer extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(getAccess());
  }

  render() {
    const { store } = this.props;

    return (
      <Provider store={store}>
        <BrowserRouter>
          <App {...this.props} />
        </BrowserRouter>
      </Provider>
    );
  }
}

function mapStateToProps(state) {
  const { isFetching, appId } = state.dbAccess;
  return {
    isFetching,
    isReady: !isFetching && !!appId
  };
}

AppContainer.propTypes = {
  store: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired
};

export default connect(mapStateToProps)(AppContainer);
