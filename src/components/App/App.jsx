import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import AccountHistory from 'components/AccountHistory';
import AccountHistoryEmpty from 'components/AccountHistoryEmpty';
import AccountList from 'components/AccountList';
import ErrorModal from 'components/ErrorModal';

import './App.css';

class App extends Component {
  render() {
    const { isFetching, isReady } = this.props;
    const classes = classNames(
      'app', {
        loading: isFetching
      }
    );

    return (
      <div className={classes}>
        {isReady &&
          <div className="d-flex h-100">
            <AccountList />
            <Switch>
              <Route path="/account/:accountId" component={AccountHistory} />
              <Route path="/" exact component={AccountHistoryEmpty} />
              <Redirect to="/" />
            </Switch>
          </div>
        }
        <ErrorModal />
      </div>
    );
  }
}

App.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  isReady: PropTypes.bool.isRequired
};

export default App;
