import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

import './ErrorModal.css';

class ErrorModal extends Component {
  render() {
    const { msg, close } = this.props;

    return (
      <Modal isOpen={!!msg} toggle={close} className="error-modal">
        <ModalHeader toggle={close}>An error has occured</ModalHeader>
        <ModalBody>{msg}</ModalBody>
      </Modal>
    );
  }
}

ErrorModal.propTypes = {
  msg: PropTypes.string,
  close: PropTypes.func.isRequired
};

export default ErrorModal;
