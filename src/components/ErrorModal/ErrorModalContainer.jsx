import { connect } from 'react-redux';
import { shiftError } from 'actions/errorList';
import ErrorModal from './ErrorModal';

function mapStateToProps(state) {
  return {
    msg: state.errorList[0]
  };
}

const mapDispatchToProps = {
  close: shiftError
};

export default connect(mapStateToProps, mapDispatchToProps)(ErrorModal);
