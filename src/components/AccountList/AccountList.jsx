import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { ListGroup } from 'reactstrap';
import AccountListItem from 'components/AccountListItem';

import './AccountList.css';

class AccountList extends Component {
  render() {
    const { isFetching, accounts, accountsById } = this.props;
    const classes = classNames(
      'account-list',
      'list-group-flush',
      'border-right', {
        loading: isFetching
      }
    );

    return (
      <ListGroup className={classes}>
        {accounts.map(accountId =>
          <AccountListItem key={accountId} account={accountsById[accountId]} />
        )}
      </ListGroup>
    );
  }
}

AccountList.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  accounts: PropTypes.arrayOf(PropTypes.string).isRequired,
  accountsById: PropTypes.objectOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired,
};

export default AccountList;
