import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { loadAccounts } from 'actions/accountList';
import AccountList from './AccountList';

class AccountListContainer extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(loadAccounts());
  }

  render() {
    return <AccountList {...this.props} />;
  }
}

function mapStateToProps(state) {
  const { isFetching, accounts, accountsById } = state.accountList;
  return {
    isFetching,
    accounts,
    accountsById
  };
}

AccountListContainer.propTypes = {
  dispatch: PropTypes.func.isRequired
};

export default withRouter(connect(mapStateToProps)(AccountListContainer));
