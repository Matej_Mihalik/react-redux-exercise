import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './AccountHistory.css';

class AccountHistory extends Component {
  render() {
    const { isFetching, id, name, history } = this.props;
    const classes = classNames(
      'account-history',
      'p-4', {
        loading: isFetching,
      }
    );

    return (
      <div className={classes}>
        {!isFetching &&
          <div>
            <h1>{name} ({id})</h1>
            <h4>Transaction history</h4>
            <table className="table table-striped table-bordered">
              <thead className="thead-light">
                <tr>
                  <th>From</th>
                  <th>To</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                {history.map(({ from, to, amount }, i) => {
                  return (
                    <tr key={i}>
                      <td>{from}</td>
                      <td>{to}</td>
                      <td>{amount.toLocaleString()}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        }
      </div>
    );
  }
}

AccountHistory.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  id: PropTypes.string,
  name: PropTypes.string,
  history: PropTypes.arrayOf(PropTypes.shape({
    from: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
    amount: PropTypes.number.isRequired
  }))
};

export default AccountHistory;
