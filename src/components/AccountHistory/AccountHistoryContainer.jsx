import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { loadAccountHistory } from 'actions/selectedAccount';
import AccountHistory from './AccountHistory';

class AccountHistoryContainer extends Component {
  componentDidMount() {
    const { dispatch, match: { params: { accountId } } } = this.props;
    dispatch(loadAccountHistory(accountId));
  }

  render() {
    return <AccountHistory {...this.props} />;
  }
}

function mapStateToProps(state) {
  const { isFetching, id, history: { op = [] } } = state.selectedAccount;
  const { name } = state.accountList.accountsById[id] || {};
  const history = op.slice(1).map(({ from, to, amount: { amount } }) => ({from, to, amount}));
  return {
    isFetching,
    id,
    name,
    history
  };
}

AccountHistoryContainer.propTypes = {
  dispatch: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      accountId: PropTypes.string.isRequired
    }).isRequired
  }).isRequired
};

export default connect(mapStateToProps)(AccountHistoryContainer);
