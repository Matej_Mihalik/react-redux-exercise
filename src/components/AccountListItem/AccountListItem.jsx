import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ListGroupItem, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';

import './AccountListItem.css';

class AccountListItem extends Component {
  render() {
    const { id, name } = this.props.account;

    return (
      <NavLink to={`/account/${id}`} className="account-list-item">
        <ListGroupItem className="list-group-item-action">
          <ListGroupItemHeading>{name}</ListGroupItemHeading>
          <ListGroupItemText className="text-muted m-0">Account ID: {id}</ListGroupItemText>
        </ListGroupItem>
      </NavLink>
    );
  }
}

AccountListItem.propTypes = {
  account: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired
};

export default AccountListItem;
