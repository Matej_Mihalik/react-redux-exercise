import { LOAD_ACCOUNTS_REQUEST, LOAD_ACCOUNTS_SUCCESS, LOAD_ACCOUNTS_FAILURE } from 'actions/types';

const defaultState = {
  isFetching: false,
  accounts: [],
  accountsById: {}
};

function mapById(accounts) {
  return accounts.reduce((accounts, account) => {
    return { ...accounts, [account.id]: account };
  }, {});
}

function extractIds(accounts) {
  return accounts.map(account => account.id);
}

function accountList(state = defaultState, action) {
  switch (action.type) {
    case LOAD_ACCOUNTS_REQUEST:
      return {
        ...state,
        isFetching: true
      };
    case LOAD_ACCOUNTS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        accounts: extractIds(action.accounts),
        accountsById: mapById(action.accounts)
      };
    case LOAD_ACCOUNTS_FAILURE:
      return {
        ...state,
        isFetching: false
      };
    default:
      return state;
  }
}

export default accountList;
