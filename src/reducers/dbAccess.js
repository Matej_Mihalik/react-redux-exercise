import { GET_DB_ACCESS_REQUEST, GET_DB_ACCESS_SUCCESS, GET_DB_ACCESS_FAILURE } from 'actions/types';

const defaultState = {
  isFetching: false,
  appId: null
};

function dbAccess(state = defaultState, action) {
  switch (action.type) {
    case GET_DB_ACCESS_REQUEST:
      return {
        ...state,
        isFetching: true
      };
    case GET_DB_ACCESS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        appId: action.appId
      };
    case GET_DB_ACCESS_FAILURE:
      return {
        ...state,
        isFetching: false
      };
    default:
      return state;
  }
}

export default dbAccess;
