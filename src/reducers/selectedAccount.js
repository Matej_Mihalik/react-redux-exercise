import { LOAD_ACCOUNT_HISTORY_REQUEST, LOAD_ACCOUNT_HISTORY_SUCCESS, LOAD_ACCOUNT_HISTORY_FAILURE } from 'actions/types';

const defaultState = {
  isFetching: false,
  id: null,
  history: []
};

function selectedAccount(state = defaultState, action) {
  switch (action.type) {
    case LOAD_ACCOUNT_HISTORY_REQUEST:
      return {
        ...state,
        isFetching: true,
        id: action.accountId
      };
    case LOAD_ACCOUNT_HISTORY_SUCCESS:
      return {
        ...state,
        isFetching: false,
        history: action.history
      };
    case LOAD_ACCOUNT_HISTORY_FAILURE:
      return {
        ...state,
        isFetching: false
      };
    default:
      return state;
  }
}

export default selectedAccount;
