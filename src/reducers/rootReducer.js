import { combineReducers } from 'redux';
import dbAccess from 'reducers/dbAccess';
import accountList from 'reducers/accountList';
import selectedAccount from 'reducers/selectedAccount';
import errorList from 'reducers/errorList';

const rootReducer = combineReducers({
  dbAccess,
  accountList,
  selectedAccount,
  errorList
});

export default rootReducer;
