import { PUSH_ERROR, SHIFT_ERROR } from 'actions/types';

const defaultState = {
  errorList: []
};

function errorList(state = defaultState, action) {
  switch (action.type) {
    case PUSH_ERROR:
      return [
        ...state,
        action.message
      ];
    case SHIFT_ERROR:
      return state.slice(1);
    default:
      return state;
  }
}

export default errorList;
