import { PUSH_ERROR, SHIFT_ERROR } from 'actions/types';

export function addError(err) {
  return dispatch => {
    const { message } = err;
    dispatch(pushError(message));
    return Promise.reject(message);
  };
}

export function pushError(message) {
  return {
    type: PUSH_ERROR,
    message
  };
}

export function shiftError() {
  return {
    type: SHIFT_ERROR
  };
}
