import { GET_DB_ACCESS_REQUEST, GET_DB_ACCESS_SUCCESS, GET_DB_ACCESS_FAILURE } from 'actions/types';
import { webSocketOpen, webSocketSend } from 'actions/webSocket';
import { addError } from 'actions/errorList';

export function getAccess() {
  return dispatch => {
    dispatch(getDbAccessRequest());
    return dispatch(webSocketOpen())
      .then(() => dispatch(login()))
      .then(() => dispatch(getApiId()))
      .then(apiId => dispatch(getDbAccessSuccess(apiId)))
      .catch(err => {
        dispatch(getDbAccessFailure());
        return Promise.reject(err);
      });
  };
}

export function login() {
  return dispatch => {
    const username = '';
    const password = '';
    const msg = [1, 'login', [username, password]];

    // Dummy data
    return new Promise((resolve, reject) => setTimeout(() => resolve({'id':1,'result':true}), 2800))
      .then(({ result }) => result ? Promise.resolve() : dispatch(addError(new Error('Login failed.'))));

    return dispatch(webSocketSend(msg))
      .then(({ result }) => result ? Promise.resolve() : dispatch(addError(new Error('Login failed.'))));
  };
}

export function getApiId() {
  return dispatch => {
    const data = [];
    const msg = [1, 'history', data];

    // Dummy data
    return new Promise((resolve, reject) => setTimeout(() => resolve({'id':2,'result':2}), 1300))
      .then(({ result }) => result ? Promise.resolve(result) : dispatch(addError(new Error('Could not get API ID.'))));

    return dispatch(webSocketSend(msg))
      .then(({ result }) => result ? Promise.resolve(result) : dispatch(addError(new Error('Could not get API ID.'))));
  };
}

export function getDbAccessRequest() {
  return {
    type: GET_DB_ACCESS_REQUEST
  };
}

export function getDbAccessSuccess(appId) {
  return {
    type: GET_DB_ACCESS_SUCCESS,
    appId
  };
}

export function getDbAccessFailure() {
  return {
    type: GET_DB_ACCESS_FAILURE
  };
}
