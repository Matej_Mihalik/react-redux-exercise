import WebSocketAsPromised from 'websocket-as-promised';
import { addError } from 'actions/errorList';

const url = 'wss://echo.websocket.org';
// const url = 'wss://stage.decentgo.com:8090';
let socket = null;

export function webSocketOpen() {
  return dispatch => {
    if(socket) {
      socket.close();
    }

    socket = new WebSocketAsPromised(url, {
      packMessage: data => JSON.stringify({ method: 'call', ...data }),
      unpackMessage: message => JSON.parse(message),
      attachRequestId: (params, id) => ({ id, params }),
      extractRequestId: ({ id }) => id
    });

    return socket.open()
      .catch(err => dispatch(addError(err)));
  };
}

export function webSocketSend(msg) {
  return dispatch => {
    if(socket) {
      return socket.sendRequest(msg)
        .catch(err => dispatch(addError(err)));
    }
    else {
      return Promise.reject('Cannot send message, socket not yet initialized.');
    }
  };
}

export function webSocketClose() {
  return dispatch => {
    if(socket) {
      return socket.close()
        .catch(err => dispatch(addError(err)));
    }
    else {
      return Promise.resolve();
    }
  };
}
