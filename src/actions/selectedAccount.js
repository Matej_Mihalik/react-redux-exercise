import { LOAD_ACCOUNT_HISTORY_REQUEST, LOAD_ACCOUNT_HISTORY_SUCCESS, LOAD_ACCOUNT_HISTORY_FAILURE } from 'actions/types';
import { webSocketSend } from 'actions/webSocket';

export function loadAccountHistory(accountId) {
  return (dispatch, getState) => {
    dispatch(loadAccountHistoryRequest(accountId));
    const apiId = getState().dbAccess.appId;
    const minId = 1;
    const maxId = 10000;
    const limit = 10;
    const msg = [apiId, 'get_relative_account_history', [accountId, minId, limit, maxId]];

    // Dummy data
    return new Promise((resolve, reject) => setTimeout(() => resolve({'id':1,'result':[{'id':'1.7.7791','op':[0,{'fee':{'amount':500000,'asset_id':'1.3.0'},'from':'1.2.75','to':'1.2.69','amount':{'amount':1000000000,'asset_id':'1.3.0'},'extensions':[]}],'result':[0,{}],'block_num':674562,'trx_in_block':0,'op_in_trx':0,'virtual_op':17698}]}), 1800))
      .then(({ result }) => {
        const history = result[0];
        return dispatch(loadAccountHistorySuccess(history));
      })
      .catch(err => {
        dispatch(loadAccountHistoryFailure());
        return Promise.reject(err);
      });

    return dispatch(webSocketSend(msg))
      .then(({ result }) => {
        const history = result[0];
        dispatch(loadAccountHistorySuccess(history));
      })
      .catch(err => {
        dispatch(loadAccountHistoryFailure());
        return Promise.reject(err);
      });
  };
}

export function loadAccountHistoryRequest(accountId) {
  return {
    type: LOAD_ACCOUNT_HISTORY_REQUEST,
    accountId
  };
}

export function loadAccountHistorySuccess(history) {
  return {
    type: LOAD_ACCOUNT_HISTORY_SUCCESS,
    history
  };
}

export function loadAccountHistoryFailure() {
  return {
    type: LOAD_ACCOUNT_HISTORY_FAILURE
  };
}
