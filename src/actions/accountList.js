import { LOAD_ACCOUNTS_REQUEST, LOAD_ACCOUNTS_SUCCESS, LOAD_ACCOUNTS_FAILURE } from 'actions/types';
import { webSocketSend } from 'actions/webSocket';

export function loadAccounts() {
  return dispatch => {
    dispatch(loadAccountsRequest());
    const name = '';
    const limit = 100;
    const msg = [0, 'lookup_accounts', [name, limit]];

    // Dummy data
    return new Promise((resolve, reject) => setTimeout(() => resolve({'id': 1,'result':[['andrew','1.2.75']]}), 4200))
      .then(({ result }) => {
        const accounts = result.map(([name, id]) => ({id, name}));
        return dispatch(loadAccountsSuccess(accounts));
      })
      .catch(err => {
        dispatch(loadAccountsFailure());
        return Promise.reject(err);
      });

    return dispatch(webSocketSend(msg))
      .then(({ result }) => {
        const accounts = result.map(([name, id]) => ({id, name}));
        dispatch(loadAccountsSuccess(accounts));
      })
      .catch(err => {
        dispatch(loadAccountsFailure());
        return Promise.reject(err);
      });
  };
}

export function loadAccountsRequest() {
  return {
    type: LOAD_ACCOUNTS_REQUEST
  };
}

export function loadAccountsSuccess(accounts) {
  return {
    type: LOAD_ACCOUNTS_SUCCESS,
    accounts
  };
}

export function loadAccountsFailure() {
  return {
    type: LOAD_ACCOUNTS_FAILURE
  };
}
