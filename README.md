# Account Viever

### What is this?
Decent React/Redux Assignment - built using React 16.3.2, Redux 4.0.0, React Router 4.2.2 and Bootstrap 4.1.1.

### How to run
#### Install dependencies
Install dependencies by running `yarn` or `npm install` from the root folder.
#### Start the app
Just run `yarn start` or `npm start` from the root folder.

Please note that internet connection IS required to run this exercise, since we need to access the testnet blockchain data in real time.
